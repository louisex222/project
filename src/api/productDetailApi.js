import { api } from "@/axios/axios";

// 詳細頁資料
const productDetailApi = {
  async fcGetProductDetail(params) {
    try {
      const url = "/productDetail";
      const res = await api.get(url, { params });
      return res.data;
    } catch (err) {
      return Promise.reject(err);
    }
  },
  async fcGetProductImg(params) {
    try {
      const url = "/productImg";
      const res = await api.get(url, { params });
      return res.data;
    } catch (err) {
      return Promise.reject(err);
    }
  },
};
export default productDetailApi;
