import { api } from "@/axios/axios";
// 產品搜尋
const productsApi = {
  async fcGetAllProducts() {
    try {
      const url = "/allProducts";
      const res = await api.get(url);
      return res.data;
    } catch (err) {
      return Promise.reject(err);
    }
  },
  async fcGetCategory() {
    try {
      const url = "/category";
      const res = await api.get(url);
      return res.data;
    } catch (err) {
      return Promise.reject(err);
    }
  },
  async fcGetCategoryProduct(params) {
    try {
      const url = "/allProducts";
      const res = await api.get(url, { params });
      return res.data;
    } catch (err) {
      return Promise.reject(err);
    }
  },
};
export default productsApi;
