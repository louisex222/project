import { api } from "@/axios/axios";

// 首頁渲染資料
const productsApi = {
  async fcGetProductlList() {
    try {
      const url = "/products";
      const res = await api.get(url);
      return res.data;
    } catch (err) {
      return Promise.reject(err);
    }
  },
  async fcGetCarouselList() {
    try {
      const url = "/carousel";
      const res = await api.get(url);
      return res.data;
    } catch (err) {
      return Promise.reject(err);
    }
  },
};
export default productsApi;
