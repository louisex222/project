import { api } from "@/axios/axios";
// 訂單頁
const orderApi = {
  // 獲取訂單
  async fcGetOrderList() {
    try {
      const url = "/order";
      const res = await api.get(url);
      return res.data;
    } catch (err) {
      return Promise.reject(err);
    }
  },
  // 新增訂單
  async fcAddOrderList(data) {
    try {
      const url = "/order";
      const res = await api.post(url, data);
      return res.data;
    } catch (err) {
      return Promise.reject(err);
    }
  },
  // 刪除訂單
  async fcDeleteOrderList(id) {
    try {
      const url = "/order/" + id;
      const res = await api.delete(url);
      return res.data;
    } catch (err) {
      return Promise.reject(err);
    }
  },
};
export default orderApi;
